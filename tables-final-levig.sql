create table levi_projects (
	projectId int auto_increment primary key,
    projectName varchar(50)
);

create table levi_employees (
	employeeId int auto_increment primary key,
    firstname varchar(50),
    lastname varchar(50)
);
    
create table levi_recordingType (
	recordingTypeId int auto_increment primary key,
    recordingTypeName varchar(50)
);
    
    
create table levi_timeRecordings (
	timeRecordingId int auto_increment primary key,
    timeRecordingDuration decimal(3,1),
    timeRecordingDate date,
    timeRecordingComment varchar(50),
    projectId int,
    employeeId int,
    recordingTypeId int
);
    
create table levi_statistics (
	statId int auto_increment primary key,
    projectId int, 
    statTypeId int,
    statDate date
);

create table levi_statisticsType (
	id int auto_increment primary key,
    statTypeName varchar(15) 
);

insert into levi_timeRecordings (timeRecordingId, timeRecordingDuration, timeRecordingDate, timeRecordingComment, projectId, employeeId, recordingTypeId) values (null, 3.0, '1899-11-30', 'Test edit', 2, 1, 2); 
insert into levi_timeRecordings (timeRecordingId, timeRecordingDuration, timeRecordingDate, timeRecordingComment, projectId, employeeId, recordingTypeId) values (null, 5.0, '2024-05-30', 'Frontend', 3, 2, 3); 
insert into levi_timeRecordings (timeRecordingId, timeRecordingDuration, timeRecordingDate, timeRecordingComment, projectId, employeeId, recordingTypeId) values (null, 7.0, '2024-06-09', 'Fertig', 1, 4, 4); 
insert into levi_timeRecordings (timeRecordingId, timeRecordingDuration, timeRecordingDate, timeRecordingComment, projectId, employeeId, recordingTypeId) values (null, 1.0, '2024-05-14', 'Zeug', 4, 3, 1); 
insert into levi_timeRecordings (timeRecordingId, timeRecordingDuration, timeRecordingDate, timeRecordingComment, projectId, employeeId, recordingTypeId) values (null, 2.0, '2024-05-31', 'Alles laeuft', 2, 2, 3); 
insert into levi_timeRecordings (timeRecordingId, timeRecordingDuration, timeRecordingDate, timeRecordingComment, projectId, employeeId, recordingTypeId) values (null, 1.0, '0000-00-00', 'schoen', 2, 4, 4); 

insert into levi_statistics (statId, projectId, statTypeId, statDate) values (null, 2, 1, '2024-05-28'); 
insert into levi_statistics (statId, projectId, statTypeId, statDate) values (null, 3, 2, '2024-05-30'); 
insert into levi_statistics (statId, projectId, statTypeId, statDate) values (null, 3, 1, '2024-05-30'); 

insert into levi_employees (employeeId, firstname, lastname) values (null, 'John', 'Doe'); 
insert into levi_employees (employeeId, firstname, lastname) values (null, 'Simeon', 'Stix'); 
insert into levi_employees (employeeId, firstname, lastname) values (null, 'Martin', 'Hager'); 
insert into levi_employees (employeeId, firstname, lastname) values (null, 'Max', 'Foll'); 
insert into levi_employees (employeeId, firstname, lastname) values (null, 'Sinan', 'Menolfi'); 

insert into levi_projects (projectId, projectName) values (null, 'Project 1'); 
insert into levi_projects (projectId, projectName) values (null, 'Project 2'); 
insert into levi_projects (projectId, projectName) values (null, 'Project 3'); 
insert into levi_projects (projectId, projectName) values (null, 'Project 4'); 

insert into levi_recordingType (recordingTypeId, recordingTypeName) values (null, 'Entwicklung');
insert into levi_recordingType (recordingTypeId, recordingTypeName) values (null, 'Debugging');
insert into levi_recordingType (recordingTypeId, recordingTypeName) values (null, 'Testing');
insert into levi_recordingType (recordingTypeId, recordingTypeName) values (null, 'Fertigstellung');

insert into levi_statisticsType (id, statTypeName) values (null, 'deleted');
insert into levi_statisticsType (id, statTypeName) values (null, 'new');
insert into levi_statisticsType (id, statTypeName) values (null, 'edited');

