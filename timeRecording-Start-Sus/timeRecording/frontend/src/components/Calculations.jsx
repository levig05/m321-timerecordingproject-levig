import React, { useState } from "react";
import calculationService from "../services/calculationService.js";

const Calculations = ({ employees, projects, recordingTypes }) => {
    const [filterType, setFilterType] = useState(''); // 'employee' oder 'recordingType'
    const [employeeId, setEmployeeId] = useState('');
    const [projectId, setProjectId] = useState('');
    const [recordingTypeId, setRecordingTypeId] = useState('');
    const [startDate, setStartDate] = useState('');
    const [endDate, setEndDate] = useState('');
    const [totalTime, setTotalTime] = useState(null);

    const handleSubmitCalc = async (event) => {
        event.preventDefault();
        //console.log(typeof recordingTypeId); 
        //console.log(projectId, filterType === 'employee' ? employeeId : recordingTypeId)
        try {
            let totalTimeResponse;
            if (projectId && startDate && endDate) {
                totalTimeResponse = await calculationService.calculateTotalTimeProject(projectId, startDate, endDate, filterType, filterType === 'employee' ? employeeId : recordingTypeId);
                //console.log(totalTimeResponse); 
                setTotalTime(totalTimeResponse.totalTime);
            } else {
                console.error("Bitte füllen Sie alle Felder aus.");
            }
        } catch (error) {
            console.error("Fehler bei der Berechnung der Gesamtzeit:", error);
        }
    };

    return (
        <div>
            <form onSubmit={handleSubmitCalc}>
                <div>
                    <label htmlFor="projectSelectForCalculations">Projekt auswählen:</label>
                    <select 
                        id="projectSelectForCalculations"
                        value={projectId}
                        onChange={(e) => setProjectId(e.target.value)}
                    >
                        <option value="">Wählen Sie ein Projekt</option>
                        {projects.map((project) => (
                            <option key={project.projectId} value={project.projectId}>
                                {project.projectName}
                            </option>
                        ))}
                    </select>
                    <br />
                    <label htmlFor="filterTypeSelect">Filtertyp auswählen:</label>
                    <select 
                        id="filterTypeSelect"
                        value={filterType}
                        onChange={(e) => setFilterType(e.target.value)}
                    >
                        <option value="">Wählen Sie einen Filtertyp</option>
                        <option value="employee">Mitarbeiter</option>
                        <option value="recordingType">Erfassungsart</option>
                    </select>
                    <br />
                    {filterType === 'employee' && (
                        <div>
                            <label htmlFor="employeeSelectForCalculations">Mitarbeiter auswählen:</label>
                            <select 
                                id="employeeSelectForCalculations"
                                value={employeeId}
                                onChange={(e) => setEmployeeId(e.target.value)}
                            >
                                <option value="all">Wählen Sie einen Mitarbeiter</option>
                                {employees.map((employee) => (
                                    <option key={employee.employeeId} value={employee.employeeId}>
                                        {employee.firstname} {employee.lastname}
                                    </option>
                                ))}
                            </select>
                            <br />
                        </div>
                    )}
                    {filterType === 'recordingType' && (
                        <div>
                            <label htmlFor="recordingTypeSelectForCalculations">Erfassungsart auswählen:</label>
                            <select 
                                id="recordingTypeSelectForCalculations"
                                value={recordingTypeId}
                                onChange={(e) => setRecordingTypeId(e.target.value)}
                            >
                                <option value="all">Wählen Sie eine Erfassungsart</option>
                                {recordingTypes.map((type) => (
                                    <option key={type.recordingTypeId} value={type.recordingTypeId}>
                                        {type.recordingTypeName}
                                    </option>
                                ))}
                            </select>
                            <br />
                        </div>
                    )}
                </div>
                <div>
                    <label htmlFor="start-date">Startdatum:</label>
                    <input 
                        type="date" 
                        id="start-date" 
                        value={startDate}
                        onChange={(e) => setStartDate(e.target.value)}
                    />
                </div>
                <div>
                    <label htmlFor="end-date">Enddatum:</label>
                    <input 
                        type="date" 
                        id="end-date" 
                        value={endDate}
                        onChange={(e) => setEndDate(e.target.value)}
                    />
                </div>
                <button type="submit">Submit</button>
            </form>
            {totalTime !== null && (
                <div>
                    <h3>Gesamte Zeit: {totalTime} Stunden</h3>
                </div>
            )}
        </div>
    );
};

export default Calculations;
