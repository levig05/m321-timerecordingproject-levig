import React, { useState } from 'react';
import TimeRecordingData from './TimeRecordingData';
import TimeRecordingSearch from './TimeRecordingSearch';
import EntryPage from './EntryPage';
import Calculations from './Calculations'; 
import Statistic from './Statistic';

const MenuComponent = ({ employees, projects, recordingTypes, timeRecording, setTimeRecording }) => {
  const [selectedComponent, setSelectedComponent] = useState('entry');

  const handleMenuClick = (component) => {
    setSelectedComponent(component);
  };

  return (
    <div>
      <div className="menu">
      <ul>
        <li className={selectedComponent === 'entry' ? 'active' : ''} onClick={() => handleMenuClick('entry')}>Einstiegsseite</li>
        <li className={selectedComponent === 'data' ? 'active' : ''} onClick={() => handleMenuClick('data')}>Zeiterfassung</li>
        <li className={selectedComponent === 'search' ? 'active' : ''} onClick={() => handleMenuClick('search')}>Suche</li>
        <li className={selectedComponent === 'calculations' ? 'active' : ''} onClick={() => handleMenuClick('calculations')}>Berechnungen</li>
        <li className={selectedComponent === 'statistics' ? 'active' : ''} onClick={() => handleMenuClick('statistics')}>Statistiken</li>
      </ul>
      </div>
      {selectedComponent === 'entry' && <EntryPage />}
      {selectedComponent === 'data' && <TimeRecordingData employees={employees} projects={projects} recordingTypes={recordingTypes}
        timeRecording={timeRecording} setTimeRecording={setTimeRecording}/>}
      {selectedComponent === 'search' && <TimeRecordingSearch projects={projects} setTimeRecording={setTimeRecording} handleMenuClick={handleMenuClick}/>}
      {selectedComponent === 'calculations' && <Calculations employees={employees} projects={projects} recordingTypes={recordingTypes}
        timeRecording={timeRecording} setTimeRecording={setTimeRecording} handleMenuClick={handleMenuClick}/>}
      {selectedComponent === 'statistics' && <Statistic projects={projects} handleMenuClick={handleMenuClick}/>}
    
    </div>
  );
};

export default MenuComponent;