import React, { useState } from "react";
import statisticsService from "../services/statisticsService.js";

const Statistic = ({ projects }) => {
    const [projectId, setProjectId] = useState('');
    const [startDate, setStartDate] = useState('');
    const [endDate, setEndDate] = useState('');
    const [statisticsTypeId, setStatisticsTypeId] = useState(''); 
    const [numberOfRecordings, setNumberOfRecordings] = useState(null);

    const handleSubmit = async (event) => {
        event.preventDefault();
        try {
            let response = await statisticsService.getStatistics(projectId, startDate, endDate, statisticsTypeId);
            setNumberOfRecordings(response.numberOfRecordings);
        } catch (error) {
            console.error("Error fetching statistics:", error);
            setNumberOfRecordings(null);
        }
        
    };

    return (
        <div>
            <form onSubmit={handleSubmit}>
                <div>
                    <label htmlFor="projectSelectForStatistics">Projekt auswählen:</label>
                    <select 
                        id="projectSelectForStatistics"
                        value={projectId}
                        onChange={(e) => setProjectId(e.target.value)}
                    >
                        <option value="">Wählen Sie ein Projekt</option>
                        {projects.map((project) => (
                            <option key={project.projectId} value={project.projectId}>
                                {project.projectName}
                            </option>
                        ))}
                    </select>
                    <br />
                    <label htmlFor="statisticTypeSelect">Art von Erfassungen auswählen:</label>
                    <select 
                        id="statisticTypeSelect"
                        value={statisticsTypeId}
                        onChange={(e) => setStatisticsTypeId(e.target.value)}
                    >
                        <option value="">Wählen Sie ein Projekt</option>
                            <option value={1}>
                                {"Gelöschte Erfassungen"}
                            </option>
                            <option value={2}>
                                {"Neue Erfassungen"}
                            </option>
                            <option value={3}>
                                {"Bearbeitete Erfassungen"}
                            </option>
                    </select>
                </div>
                <div>
                    <label htmlFor="start-date">Startdatum:</label>
                    <input 
                        type="date" 
                        id="start-date" 
                        value={startDate}
                        onChange={(e) => setStartDate(e.target.value)}
                    />
                </div>
                <div>
                    <label htmlFor="end-date">Enddatum:</label>
                    <input 
                        type="date" 
                        id="end-date" 
                        value={endDate}
                        onChange={(e) => setEndDate(e.target.value)}
                    />
                </div>
                <button type="submit">Submit</button>
            </form>
            {numberOfRecordings !== null && (
                <div>
                    <h3>Anzahl der Aufzeichnungen: {numberOfRecordings}</h3>
                </div>
            )}
        </div>
    );
};

export default Statistic;
