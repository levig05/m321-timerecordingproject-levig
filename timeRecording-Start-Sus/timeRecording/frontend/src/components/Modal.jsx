import React, { useState } from 'react';

const ErrorPopup = ({ error, onClose }) => {
  return (
    <div className="error-popup">
      <div className="error-popup-content">
        <p>An error occurred: {error}</p>
        <button onClick={onClose}>Close</button>
      </div>
    </div>
  );
};

const Modal = () => {
  const [showErrorPopup, setShowErrorPopup] = useState(false);
  const [errorMessage, setErrorMessage] = useState('');

  const handleInitKeycloak = () => {
    authenticationService.initKeycloak()
      .then((token) => {
        console.log("Token received: ", token);
        setAuthorizationToken(token);
      })
      .catch((error) => {
        console.log("Error in initKeycloak App.jsx:", error);
        setErrorMessage(error);
        setShowErrorPopup(true);
      });
  };

  const closeErrorPopup = () => {
    setShowErrorPopup(false);
  };

  return (
    <div>
      {showErrorPopup && <ErrorPopup error={errorMessage} onClose={closeErrorPopup} />}
      <button onClick={handleInitKeycloak}>Initialize Keycloak</button>
      {/* Other content */}
    </div>
  );
};

export default Modal;
