import React, { useState, useEffect } from "react";

import timeRecordingDataService from '../services/timeRecordingDataService'

const TimeRecordingData = ({ employees, projects, recordingTypes, timeRecording, setTimeRecording }) => {
    // all properties of the form
    // only the id of employee, project and recordingType is used / saved
    const [employeeId, setEmployeeId] = useState('');
    const [projectId, setProjectId] = useState('');
    const [recordingTypeId, setRecordingTypeId] = useState('');
    const [timeRecordingDate, setDate] = useState('');
    const [timeRecordingDuration, setDuration] = useState('');
    const [timeRecordingComment, setComment] = useState('');

    useEffect(() => {
        // set selected values for dropdowns
        setEmployeeId(timeRecording.employeeId);
        setProjectId(timeRecording.projectId);
        setRecordingTypeId(timeRecording.recordingTypeId);
        // set ohter values
        setComment(timeRecording.timeRecordingComment);
        setDate(timeRecording.timeRecordingDate);
        setDuration(timeRecording.timeRecordingDuration);
    }, []); // should render at every button click, e.g. edit

    /*const addEditTimeRecording = (event) => {
        event.preventDefault();
        console.log('addEditTimeRecording');
        const timeRecordingToAdd = { 
            projectId: projectId, 
            employeeId: employeeId, 
            recordingTypeId: recordingTypeId, 
            date: date, 
            duration: duration, 
            comment: comment 
        };
        if (timeRecording.timeRecordingId > 0) {
            // edit
            const timeRecordingToEdit = { ...timeRecordingToAdd, timeRecordingId: timeRecording.timeRecordingId }
            timeRecordingDataService.editTimeRecording(timeRecordingToEdit).then(id => {
                console.log('time recording edited with id', id);
                setTimeRecording(timeRecordingToEdit);
            })
        } else {
            // add
            timeRecordingDataService.addTimeRecording(timeRecordingToAdd).then(id => {
                console.log('time recording added with id', id);
                timeRecordingToAdd.timeRecordingId = id;
                setTimeRecording(timeRecordingToAdd);
            })
        }
    }*/

    const addEditTimeRecording = (event) => {
        event.preventDefault();
        console.log('addEditTimeRecording');
        const timeRecordingToAdd = { 
            projectId: projectId, 
            employeeId: employeeId, 
            recordingTypeId: recordingTypeId, 
            date: timeRecordingDate, 
            duration: timeRecordingDuration, 
            comment: timeRecordingComment 
        };
    
        if (timeRecording.timeRecordingId > 0) {
            // edit
            const timeRecordingToEdit = { ...timeRecordingToAdd, timeRecordingId: timeRecording.timeRecordingId };
            console.log('Editing time recording:', timeRecordingToEdit);
            timeRecordingDataService.editTimeRecording(timeRecordingToEdit)
                .then(id => {
                    console.log('Time recording edited with id:', id);
                    setTimeRecording(timeRecordingToEdit);
                })
                .catch(error => console.error('Error editing time recording:', error));
        } else {
            // add
            console.log('Adding new time recording:', timeRecordingToAdd);
            timeRecordingDataService.addTimeRecording(timeRecordingToAdd)
                .then(id => {
                    console.log('Time recording added with id:', id);
                    timeRecordingToAdd.timeRecordingId = id;
                    setTimeRecording(timeRecordingToAdd);
                })
                .catch(error => console.error('Error adding time recording:', error));
        }
    }
    



    const handleCommentChange = (event) => {
        let timeRecordingComment = event.target.value;
        console.log('handleCommentChange', timeRecordingComment);
        setComment(timeRecordingComment);
    };

    const handleDurationChange = (event) => {
        let timeRecordingDuration = event.target.value;
        console.log('handleDurationChange', timeRecordingDuration);
        setDuration(timeRecordingDuration);
    };

    const handleDateChange = (event) => {
        let timeRecordingDate = event.target.value;
        console.log('handleDateChange', timeRecordingDate);
        setDate(timeRecordingDate);
    };

    const handleEmployeeChange = (event) => {
        let employeeId = event.target.value;
        console.log('handleEmployeeChange', employeeId);
        setEmployeeId(employeeId);
    }

    const handleProjectChange = (event) => {
        let projectId = event.target.value;
        console.log('handleProjecthange', projectId);
        setProjectId(projectId);
    }

    const handleRecordingTypeChange = (event) => {
        let recordingTypeId = event.target.value;
        console.log('handleRecordingTypeChange', recordingTypeId);
        setRecordingTypeId(recordingTypeId);
    }

    return (
        <form onSubmit={addEditTimeRecording}>
            <div>
                <label>
                Projekt: 
                <select
                    onChange={handleProjectChange}
                    value={projectId} >
                    <option value="" disabled>Projekt auswählen</option>
                    {projects.map(project => (
                        <option key={project.projectId} value={project.projectId} >{project.projectName} </option>
                    ))}
                </select>
                </label>
            </div>
            <div><label>Art der Zeiterfassung: 
                <select
                    onChange={handleRecordingTypeChange}
                    value={recordingTypeId} >
                    <option value="" disabled>Zeiterfassungsart auswählen</option>
                    {recordingTypes.map(type => (
                        <option key={type.recordingTypeId} value={type.recordingTypeId}>{type.recordingTypeName}</option>
                    ))}
                </select>
                </label>
            </div>
            <div>
                <label>
                Mitarbeiter: 
                <select
                    onChange={handleEmployeeChange}
                    value={employeeId} >
                    <option value="" disabled>Mitarbeiter auswählen</option>
                    {employees.map(employee => (
                        <option key={employee.employeeId} value={employee.employeeId}>{employee.firstname} {employee.lastname}</option>
                    ))}
                </select>
                </label>
            </div>
            <div>
                <label>
                    Datum:
                    <input
                        type="date"
                        value={timeRecordingDate}
                        onChange={handleDateChange}
                    />
                </label>
            </div>
            <div>
                <label>
                    Zeit in Stunden:
                    <input
                        type="number"
                        value={timeRecordingDuration}
                        onChange={handleDurationChange}
                        placeholder="0.0"
                    />
                </label>
            </div>
            <div>
                <label>
                    Bemerkung:
                    <input
                        type="text"
                        value={timeRecordingComment}
                        onChange={handleCommentChange}
                        placeholder="Text hier"
                    />
                </label>
            </div>
            <div>
                <button style={{ margin: '5px' }} type="submit">Speichern</button>
            </div>
        </form>
    )

}

export default TimeRecordingData;