import React, { useState } from "react";
import timeRecordingDataService from '../services/timeRecordingDataService';

const TimeRecordingSearch = ({ projects, setTimeRecording , handleMenuClick}) => {
    const [projectId, setProjectId] = useState('');
    const [timeRecordings, setTimeRecordings] = useState([]);

    const searchTimeRecordings = (event) => {
        event.preventDefault();
        console.log('searchTimeRecordings');
        timeRecordingDataService.searchTimeRecordings(projectId)
            .then(timeRecordingsFound => {
                console.log('time recordings found', timeRecordingsFound);
                setTimeRecordings(timeRecordingsFound);
            })
            .catch(error => console.error("Error searching time recordings:", error));
    };

    const handleProjectChange = (event) => {
        let id = event.target.value;
        console.log('handleProjectChange', id);
        setProjectId(id);
    };

    const handleEditTimeRecording = (timeRecording) => {
        console.log("Edit time recording:", timeRecording);
        setTimeRecording(timeRecording);
        handleMenuClick('data');
    };

    const handleDeleteTimeRecording = (id) => {
        console.log("Delete time recording with id:", id);
        timeRecordingDataService.deleteTimeRecording(id)
            .then(() => {
                // Update timeRecordings after deletion
                searchTimeRecordings();
            })
            .catch(error => console.error("Error deleting time recording:", error));
    };

    const formatDate = (date) => {
        const newdate = new Date(date);
        const day = newdate.getDate();
        const month = newdate.getMonth() + 1;
        const year = newdate.getFullYear();

    
        const formattedDay = (day < 10) ? '0' + day : day;
        const formattedMonth = (month < 10) ? '0' + month : month;

        return formattedDay + '.' + formattedMonth + '.' + year;
    };

    return (
        <div>
            <form onSubmit={searchTimeRecordings}>
                <div>
                    <label>Projekt:
                        <select
                            onChange={handleProjectChange}
                            value={projectId} >
                            <option value="" disabled>Projekt auswählen</option>
                            {projects.map(project => (
                                <option key={project.projectId} value={project.projectId}>{project.projectName}</option>
                            ))}
                        </select>
                    </label>
                </div>
                <div style={{ margin: '5px' }}>
                    <button type="submit">Suchen</button>
                </div>
            </form>
            {/* Show time recordings in a table/list */}
            {timeRecordings.length > 0 && (
                <div>
                    <label>
                    <h3>Gefundene Zeitdaten:</h3>
                    <table>
                        <thead>
                            <tr>
                                <th>Time Recording ID</th>
                                <th>Projekt</th>
                                <th>Mitarbeiter</th>
                                <th>Recording Type ID</th>
                                <th>Dauer</th>
                                <th>Kommentar</th>
                                <th>Datum</th>
                                <th>Aktionen</th>
                            </tr>
                        </thead>
                        <tbody>
                            {timeRecordings.map(timeRecording => (
                                <tr key={timeRecording.timeRecordingId}>
                                    <td>{timeRecording.timeRecordingId}</td>
                                    <td>{timeRecording.projectId}</td>
                                    <td>{timeRecording.employeeId}</td>
                                    <td>{timeRecording.recordingTypeId}</td>
                                    <td>{timeRecording.timeRecordingDuration}</td>
                                    <td>{timeRecording.timeRecordingComment}</td>
                                    <td>{formatDate(timeRecording.timeRecordingDate)}</td>
                                    <td>
                                        <button onClick={() => handleEditTimeRecording(timeRecording)}>Edit</button>
                                        <button onClick={() => handleDeleteTimeRecording(timeRecording.timeRecordingId)}>Delete</button>
                                    </td>
                                </tr>
                            ))}
                        </tbody>
                    </table>
                    </label>
                </div>
            )}
        </div>
    );
};






export default TimeRecordingSearch;
