import React from 'react';

const EntryPage = () => {
  return (
    <div>
      <label>
        <h1>Zeiterfassung, Berechnungen und Statistiken M321</h1>
        <p>Es können geleistete Stunden für ein Projekt erfasst und verwaltet werden. Es gibt verschiedene Berechnungen und Statistiken.</p>
        <p>Die Authentisierung fürs Front- und Backend geschieht mit Keycloak.</p>
      </label>
    </div>
  );
};

export default EntryPage;