import React, { useState, useEffect } from "react";
import './App.css';
import TimeRecordingData from './components/TimeRecordingData';
import TimeRecordingSearch from './components/TimeRecordingSearch';
import miscService from './services/timeRecordingMiscService';
import authenticationService from "./services/authenticationService";
import Modal from './components/Modal.jsx';
import MenuComponent from "./components/MenuComponent.jsx";
import EntryPage from "./components/EntryPage.jsx";


function App() {
  const [employees, setEmployees] = useState([])
  const [projects, setProjects] = useState([])
  const [recordingTypes, setRecordingTypes] = useState([])
  const [token, setAuthorizationToken] = useState()
  const [timeRecording, setTimeRecording] = useState({
    projectId: "2",
    employeeId: "1",
    recordingTypeId: "2",
    date: new Date().toISOString().split('T')[0],
    duration: 4,
    comment: "Comment"
  });

  useEffect(() => {
    
    authenticationService.initKeycloak().then((token)=>{
      //console.log("token recieved: ", token)
      setAuthorizationToken(token); 
      readProjects();
      readEmployees();
      readRecordingTypes();
    }).catch((error)=>{
      console.log("initKeyCloak App.jsx", error)
      
    })  
    
    
    
  }, []);

  const readEmployees = () => {
    miscService.readAllEmployees()
      .then(data => {
        console.log('readEmployees in App.jsx: ', data);
        setEmployees(data);
      })
      .catch(error => console.error("Error reading employees:", error));
  };

  const readRecordingTypes = () => {
    miscService.readAllRecordingTypes()
      .then(data => {
        console.log('readRecordingTypes in App.jsx: ', data);
        setRecordingTypes(data);
      })
      .catch(error => console.error("Error reading recording types:", error));
  };

  const readProjects = () => {
    miscService.readAllProjects()
      .then(data => {
        console.log('readProjects in App.jsx: ', data);
        setProjects(data);
      })
      .catch(error => console.error("Error reading projects:", error));
  };

  return (
    <div>
      <MenuComponent employees={employees} projects={projects} recordingTypes={recordingTypes}
        timeRecording={timeRecording} setTimeRecording={setTimeRecording} />
    </div>
  )
}

/*
<TimeRecordingData employees={employees} projects={projects} recordingTypes={recordingTypes}
        timeRecording={timeRecording} setTimeRecording={setTimeRecording} />
      <h3>Suche</h3>
      <TimeRecordingSearch projects={projects} setTimeRecording={setTimeRecording} />
*/


export default App
