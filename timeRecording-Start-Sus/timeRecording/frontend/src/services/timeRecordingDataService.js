import axios from "axios";
import authenticationService from "./authenticationService";

const API_URL = "http://localhost:3000/api/";
const baseUrlData = API_URL + "data/";

const addTimeRecording = async (timeRecording) => {
  let token = await authenticationService.getToken();
  const url = baseUrlData + "add";
  try {
    const response = await axios.post(url, timeRecording, {
      headers: {
        authorization: `Bearer ${token}`,
      },
    });
    return response.data;
  } catch (error) {
    console.error("Error adding time recording:", error);
    throw error;
  }
};

const editTimeRecording = async (timeRecording) => {
  let token = await authenticationService.getToken();
  const url = baseUrlData + "edit";
  try {
    const response = await axios.put(url, timeRecording, {
      headers: {
        authorization: `Bearer ${token}`,
      },
    });
    return response.data;
  } catch (error) {
    console.error("Error editing time recording:", error);
    throw error;
  }
};

const deleteTimeRecording = async (timeRecordingId) => {
  let token = await authenticationService.getToken();
  const url = baseUrlData + "delete/" + timeRecordingId;
  try {
    const response = await axios.delete(url, {
      headers: {
        authorization: `Bearer ${token}`,
      },
    });
    return response.data;
  } catch (error) {
    console.error("Error deleting time recording:", error);
    throw error;
  }
};

const searchTimeRecordings = async (projectId) => {
  let token = await authenticationService.getToken();
  const url = baseUrlData + "search/" + projectId;
  try {
    const response = await axios.get(url, {
      headers: {
        authorization: `Bearer ${token}`,
      },
    });
    //console.log(response.data);
    return response.data;
  } catch (error) {
    console.error("Error searching time recordings:", error);
    throw error;
  }
};

export default {
  addTimeRecording,
  editTimeRecording,
  searchTimeRecordings,
  deleteTimeRecording,
};
