import axios from "axios";
import authenticationService from "./authenticationService";

const baseUrlData = "http://localhost:3000/api/misc/";

const readAllEmployees = async () => {
  let token = await authenticationService.getToken();
  const url = baseUrlData + "employee/";
  try {
    const response = await axios.get(url, {
      headers: {
        authorization: `Bearer ${token}`,
      },
    });
    return response.data;
  } catch (error) {
    console.error("Error reading all employees:", error);
    throw error;
  }
};

const readAllProjects = async () => {
  let token = await authenticationService.getToken();
  //console.log("readAllProjects service with token", token);
  const response = axios.get(baseUrlData + "project/", {
    headers: {
      authorization: `Bearer ${token}`,
    },
  });
  return response.then((res) => {
    let data = res.data;
    //console.log("readAllProjects service", data);

    return data;
  });
};

const readAllRecordingTypes = async () => {
  let token = await authenticationService.getToken();
  const url = baseUrlData + "type/";
  try {
    const response = await axios.get(url, {
      headers: {
        authorization: `Bearer ${token}`,
      },
    });
    return response.data;
  } catch (error) {
    console.error("Error reading all recording types:", error);
    throw error;
  }
};

export default {
  readAllEmployees,
  readAllProjects,
  readAllRecordingTypes,
};
