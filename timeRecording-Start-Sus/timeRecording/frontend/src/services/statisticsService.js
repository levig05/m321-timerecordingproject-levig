import axios from "axios";
import authenticationService from "./authenticationService";

const API_URL = "http://localhost:3000/api/";
const baseUrlStat = API_URL + "stat/";

const getStatistics = async (
  projectId,
  startDate,
  endDate,
  statisticsTypeId
) => {
  let token = await authenticationService.getToken();
  let url = "";
  //console.log(typeof statisticsTypeId);
  if (statisticsTypeId === "1") {
    url =
      baseUrlStat +
      "deletedRecords/" +
      projectId +
      "/" +
      startDate +
      "/" +
      endDate;
  } else if (statisticsTypeId === "2") {
    url =
      baseUrlStat + "newRecords/" + projectId + "/" + startDate + "/" + endDate;
  } else if (statisticsTypeId === "3") {
    url =
      baseUrlStat +
      "editedRecords/" +
      projectId +
      "/" +
      startDate +
      "/" +
      endDate;
  } else {
    return;
  }
  try {
    const response = await axios.get(url, {
      headers: {
        authorization: `Bearer ${token}`,
      },
    });
    //console.log(response.data);
    return response.data;
  } catch (error) {
    console.error("Error searching time recordings:", error);
    throw error;
  }
};

function testFunction() {}

export default { getStatistics, testFunction };
