import axios from "axios";
import authenticationService from "./authenticationService";

const API_URL = "http://localhost:3000/api/";
const baseUrlCalc = API_URL + "calc/";

const calculateTotalTimeProject = async (
  projectId,
  startDate,
  endDate,
  filterType,
  empOrTypeId
) => {
  //console.log("hello world", empOrTypeId === "" ? "true" : "false");
  let token = await authenticationService.getToken();
  let calcUrl;
  if (filterType === "employee") {
    if (empOrTypeId === "") {
      empOrTypeId = "all";
      calcUrl =
        baseUrlCalc +
        "totalTimeProject/all/" +
        projectId +
        "/" +
        startDate +
        "/" +
        endDate;
    } else {
      calcUrl =
        baseUrlCalc +
        "totalTimeProject/employee/" +
        projectId +
        "/" +
        startDate +
        "/" +
        endDate +
        "/" +
        empOrTypeId;
    }
  } else if (filterType === "recordingType") {
    if (empOrTypeId === "") {
      empOrTypeId = "all";
      calcUrl =
        baseUrlCalc +
        "totalTimeProject/all/" +
        projectId +
        "/" +
        startDate +
        "/" +
        endDate;
    } else {
      calcUrl =
        baseUrlCalc +
        "totalTimeProject/recordingType/" +
        projectId +
        "/" +
        startDate +
        "/" +
        endDate +
        "/" +
        empOrTypeId;
    }
  } else {
    calcUrl =
      baseUrlCalc +
      "totalTimeProject/all/" +
      projectId +
      "/" +
      startDate +
      "/" +
      endDate;
  }

  try {
    const response = await axios.get(calcUrl, {
      headers: {
        authorization: `Bearer ${token}`,
      },
    });

    return response.data;
  } catch (error) {
    console.error("Error searching time recordings:", error);
    throw error;
  }
};

export default { calculateTotalTimeProject };
