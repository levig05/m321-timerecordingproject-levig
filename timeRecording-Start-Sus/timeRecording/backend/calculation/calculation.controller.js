// calculation.controller.js

import {
  getTotalTimeForProjectWEmployee,
  getTotalTimeForProjectWRecordingType,
  getTotalTimeForProjectWAll,
} from "./calculation.model.js";

/*
async function calculateTime(req, res) {
  const { projectId, startDate, endDate } = req.params;

  try {
    // Übergeben Sie die Parameter an das Model, um die Zeit zu berechnen
    const totalTime = await calculateTimeModel.getTotalTimeForProject(
      projectId,
      startDate,
      endDate
    );

    // Wenn die Berechnung erfolgreich ist, senden Sie die Zeit als Antwort
    res.json({ totalTime });
  } catch (error) {
    // Wenn ein Fehler auftritt, senden Sie eine Fehlermeldung zurück
    res.status(500).json({
      error: "Fehler bei der Berechnung der Gesamtzeit für das Projekt",
    });
  }
}
*/

async function calculateTimeWE(req, res) {
  const { projectId, startDate, endDate, empOrTypeId } = req.params;
  try {
    const dataLines = await getTotalTimeForProjectWEmployee(
      projectId,
      startDate,
      endDate,
      empOrTypeId
    );
    const totalTime = dataLines.reduce((sum, dataLine) => {
      return (sum += parseInt(dataLine.timeRecordingDuration));
    }, 0);
    res.json({ totalTime });
  } catch (error) {
    res.status(500).json({
      error: "Fehler bei der Berechnung der Gesamtzeit für das Projekt",
    });
  }
}

async function calculateTimeWRT(req, res) {
  const { projectId, startDate, endDate, empOrTypeId } = req.params;
  //console.log("hello world", empOrTypeId);
  try {
    const dataLines = await getTotalTimeForProjectWRecordingType(
      projectId,
      startDate,
      endDate,
      empOrTypeId
    );
    const totalTime = dataLines.reduce((sum, dataline) => {
      return (sum += parseInt(dataline.timeRecordingDuration));
    }, 0);
    //console.log(totalTime);
    res.json({ totalTime });
  } catch (error) {
    res.status(500).json({
      error: "Fehler bei der Berechnung der Gesamtzeit für das Projekt",
    });
  }
}

async function calculateTimeAll(req, res) {
  const { projectId, startDate, endDate } = req.params;
  //console.log("hey from all employees");
  try {
    const dataLines = await getTotalTimeForProjectWAll(
      projectId,
      startDate,
      endDate
    );
    const totalTime = dataLines.reduce((sum, dataline) => {
      return (sum += parseInt(dataline.timeRecordingDuration));
    }, 0);
    //console.log(totalTime);
    res.json({ totalTime });
  } catch (error) {
    res.status(500).json({
      error: "Fehler bei der Berechnung der Gesamtzeit für das Projekt",
    });
  }
}

export { calculateTimeWE, calculateTimeWRT, calculateTimeAll };
