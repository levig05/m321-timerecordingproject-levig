import {
  getRecordingsForCalcWEmployee,
  getRecordingsForCalcWRecordingType,
  getAllRecordingsForCalcWithAll,
} from "../services/database.js";

function formatDate(dateString) {
  const parts = dateString.split(".");
  return new Date(parts[2], parts[1] - 1, parts[0]);
}

async function getTotalTimeForProjectWEmployee(
  projectId,
  startDate,
  endDate,
  employeeId
) {
  let ret = await getRecordingsForCalcWEmployee(
    projectId,
    startDate,
    endDate,
    employeeId
  );
  return ret;
}

async function getTotalTimeForProjectWRecordingType(
  projectId,
  startDate,
  endDate,
  recordingTypeId
) {
  let ret = await getRecordingsForCalcWRecordingType(
    projectId,
    startDate,
    endDate,
    recordingTypeId
  );
  return ret;
}

async function getTotalTimeForProjectWAll(projectId, startDate, endDate) {
  let ret = await getAllRecordingsForCalcWithAll(projectId, startDate, endDate);
  return ret;
}

export {
  getTotalTimeForProjectWEmployee,
  getTotalTimeForProjectWRecordingType,
  getTotalTimeForProjectWAll,
};
