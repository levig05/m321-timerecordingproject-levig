import { Router } from "express";
import {
  calculateTimeWE,
  calculateTimeWRT,
  calculateTimeAll,
} from "./calculation.controller.js";

//    /api/calc/totalTimeProject/:projectId/:startDate/:endDate
const router = Router();

router.get(
  "/totalTimeProject/employee/:projectId/:startDate/:endDate/:empOrTypeId",
  calculateTimeWE
);
router.get(
  "/totalTimeProject/recordingType/:projectId/:startDate/:endDate/:empOrTypeId",
  calculateTimeWRT
);
router.get(
  "/totalTimeProject/all/:projectId/:startDate/:endDate",
  calculateTimeAll
);

export { router };
