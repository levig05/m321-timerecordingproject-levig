import { selectTimeRecordingsFromStat } from "../services/database.js";

async function getDeletedRecordingsFromDB(projectId, startDate, endDate) {
  let ret = await selectTimeRecordingsFromStat(
    projectId,
    startDate,
    endDate,
    1
  );
  return ret;
}

async function getNewRecordingsFromDB(projectId, startDate, endDate) {
  let ret = await selectTimeRecordingsFromStat(
    projectId,
    startDate,
    endDate,
    2
  );
  return ret;
}

async function getEditedRecordingsFromDB(projectId, startDate, endDate) {
  let ret = await selectTimeRecordingsFromStat(
    projectId,
    startDate,
    endDate,
    3
  );
  return ret;
}

export {
  getDeletedRecordingsFromDB,
  getNewRecordingsFromDB,
  getEditedRecordingsFromDB,
};
