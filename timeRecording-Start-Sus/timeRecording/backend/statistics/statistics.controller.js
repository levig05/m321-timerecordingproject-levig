import {
  getDeletedRecordingsFromDB,
  getNewRecordingsFromDB,
  getEditedRecordingsFromDB,
} from "./statistics.model.js";

async function getNumberOfDeletedRecordings(req, res) {
  let { projectId, startDate, endDate } = req.params;
  let deletedRecordings = await getDeletedRecordingsFromDB(
    projectId,
    startDate,
    endDate
  );
  /*console.log(
    "deleted recordings: ",
    deletedRecordings,
    deletedRecordings.length
  );*/
  res.json({ numberOfRecordings: deletedRecordings.length });
}

async function getNumberOfNewRecordings(req, res) {
  let { projectId, startDate, endDate } = req.params;
  let newRecordings = await getNewRecordingsFromDB(
    projectId,
    startDate,
    endDate
  );
  //console.log("new recordings: ", newRecordings, newRecordings.length);
  res.json({ numberOfRecordings: newRecordings.length });
}

async function getNumberOfEditedRecordings(req, res) {
  let { projectId, startDate, endDate } = req.params;
  let editedRecordings = await getEditedRecordingsFromDB(
    projectId,
    startDate,
    endDate
  );
  //console.log("edited recordings: ", editedRecordings, editedRecordings.length);
  res.json({ numberOfRecordings: editedRecordings.length });
}

export {
  getNumberOfDeletedRecordings,
  getNumberOfNewRecordings,
  getNumberOfEditedRecordings,
};
