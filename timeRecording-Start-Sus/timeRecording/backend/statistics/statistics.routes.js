import { Router } from "express";
import {
  getNumberOfDeletedRecordings,
  getNumberOfNewRecordings,
  getNumberOfEditedRecordings,
} from "./statistics.controller.js";

const router = Router();

router.get(
  "/deletedRecords/:projectId/:startDate/:endDate",
  getNumberOfDeletedRecordings /*func*/
);
router.get(
  "/newRecords/:projectId/:startDate/:endDate",
  getNumberOfNewRecordings /*func*/
);
router.get(
  "/editedRecords/:projectId/:startDate/:endDate",
  getNumberOfEditedRecordings /*func*/
);

export { router };
