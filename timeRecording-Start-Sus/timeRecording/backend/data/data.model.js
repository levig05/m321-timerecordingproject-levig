import {
  editTimeRecordingModelDB,
  addTimeRecordingModelDB,
  deleteTimeRecordingModelDB,
  searchTimeRecordingsForProjectDB,
} from "../services/database.js";

const timeRecordings = [
  {
    timeRecordingId: 1,
    projectId: "1",
    employeeId: 1,
    recordingTypeId: 1,
    duration: 2.5,
    date: "08.11.2023",
    comment: "Toll",
  },
  {
    timeRecordingId: 2,
    projectId: "2",
    employeeId: 3,
    recordingTypeId: 3,
    duration: 1.5,
    date: "14.11.2023",
    comment: "Super",
  },
  {
    timeRecordingId: 3,
    projectId: "3",
    employeeId: 2,
    recordingTypeId: 4,
    duration: 4,
    date: "29.12.2023",
    comment: "Nicht ganz so gut",
  },
  {
    timeRecordingId: 4,
    projectId: "4",
    employeeId: 4,
    recordingTypeId: 2,
    duration: 4,
    date: "30.01.2024",
    comment: "Befridigend",
  },
];

//{ readEmployees, readProjects, readRecordingTypes };

function searchTimeRecordingsForProject(projectId) {
  console.log("searchTimeRecordingsForProject:", projectId);
  return searchTimeRecordingsForProjectDB(projectId);
}

function deleteTimeRecordingModel(timeRecordingId) {
  console.log("deleteTimeRecordingModel:", timeRecordingId);
  return deleteTimeRecordingModelDB(timeRecordingId);
}

function addTimeRecordingModel(timeRecording) {
  console.log("addTimeRecordingModel:", timeRecording);
  return addTimeRecordingModelDB(timeRecording);
}
function editTimeRecordingModel(timeRecording) {
  console.log("editTimeRecordingModel:", timeRecording);
  return editTimeRecordingModelDB(timeRecording);
}

export {
  addTimeRecordingModel,
  editTimeRecordingModel,
  searchTimeRecordingsForProject,
  deleteTimeRecordingModel,
};
