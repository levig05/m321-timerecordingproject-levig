import {
  addTimeRecordingModel,
  editTimeRecordingModel,
  searchTimeRecordingsForProject,
  deleteTimeRecordingModel,
} from "./data.model.js";

async function addTimeRecording(request, response) {
  let timeRecording = request.body;
  //console.log("addTimeRecording", timeRecording);
  const timeRecordingId = await addTimeRecordingModel(timeRecording);
  //console.log("timeRecording added with id", timeRecordingId);
  response.json(timeRecordingId);
}
async function editTimeRecording(request, response) {
  let timeRecording = request.body;
  //console.log("createTimeRecording", timeRecording);
  const timeRecordingId = await editTimeRecordingModel(timeRecording);
  //console.log("timeRecording edited with id", timeRecordingId);
  response.json(timeRecording);
}

async function deleteTimeRecording(request, response) {
  let id = request.params;
  //console.log("deleteTimeRecording", id);
  const timeRecordingId = await deleteTimeRecordingModel(id);
  //console.log("timeRecording deleted with id", id);
  response.json(id);
}

async function searchTimeRecordings(request, response) {
  let projectId = request.params.projectId;
  //console.log("searchTimeRecordings", request.params);
  //console.log("searchTimeRecordings", projectId);
  const timeRecordings = await searchTimeRecordingsForProject(projectId);
  response.json(timeRecordings);
}

export {
  addTimeRecording,
  editTimeRecording,
  searchTimeRecordings,
  deleteTimeRecording,
};
