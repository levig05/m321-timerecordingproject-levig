import mysql from "mysql2";
import { publishEvent } from "../rabitmq/publisher.js";

// Verbindung zur MySQL-Datenbank herstellen
const connection = mysql.createConnection({
  host: "sql11.freemysqlhosting.net",
  user: "sql11690338",
  password: "2yn4rDZNTG",
  database: "sql11690338",
});

const logEvent = (eventType, data) => {
  publishEvent({
    eventType,
    ...data,
  });
};

/*connection.connect((err) => {
  if (err) {
    console.error("Fehler beim Verbinden zur Datenbank: " + err.stack);
    return;
  }
  console.log(
    "Erfolgreich mit der MySQL-Datenbank verbunden als ID: " +
      connection.threadId
  );
});*/

function readEmployeesDB() {
  return new Promise((resolve, reject) => {
    connection.query(
      "SELECT * FROM levi_employees",
      (error, results, fields) => {
        if (error) {
          reject(error);
        } else {
          resolve(results);
        }
      }
    );
  });
}

function readProjectsDB() {
  return new Promise((resolve, reject) => {
    connection.query(
      "SELECT * FROM levi_projects",
      (error, results, fields) => {
        if (error) {
          reject(error);
        } else {
          resolve(results);
        }
      }
    );
  });
}

function readRecordingTypesDB() {
  return new Promise((resolve, reject) => {
    connection.query(
      `SELECT * FROM levi_recordingType`,
      (error, results, fields) => {
        if (error) {
          reject(error);
        } else {
          resolve(results);
        }
      }
    );
  });
}

async function searchTimeRecordingsForProjectDB(projectId) {
  /*console.log("searchTimeRecordingsForProject:", projectId);
    return timeRecordings;*/
  let projectName = await sqlSelectsForLogs(
    "projects",
    projectId,
    "projectId",
    "projectName"
  );
  projectName = projectName[0].projectName;
  //console.log(projectName);
  await logEvent("Suchen", { projectName: projectName });
  return new Promise((resolve, reject) => {
    connection.query(
      `SELECT * FROM levi_timeRecordings WHERE projectId = ${projectId}`,
      async (error, results, fields) => {
        if (error) {
          reject(error);
        } else {
          resolve(results);
        }
      }
    );
  });
}

async function getProjectIdWithTimeRecordingId(timeRecordingId) {
  return new Promise((resolve, reject) => {
    connection.query(
      `SELECT projectId FROM levi_timeRecordings WHERE timeRecordingId = ${timeRecordingId.id}`,
      (error, results, fields) => {
        if (error) {
          reject(error);
        } else {
          //console.log("hello world", results);
          resolve(results[0].projectId);
        }
      }
    );
  });
}

async function deleteTimeRecordingModelDB(timeRecordingId) {
  try {
    let timeRecording = await selectAllFromTimeRecWithId(timeRecordingId.id);
    timeRecording = timeRecording[0];
    //console.log("check if function works: ", timeRecording);

    let projectName = await sqlSelectsForLogs(
      "projects",
      timeRecording.projectId,
      "projectId",
      "projectName"
    );
    projectName = projectName[0].projectName;

    let employeeFullName = await sqlSelectsForLogs(
      "employees",
      timeRecording.employeeId,
      "employeeId",
      "firstname, lastname"
    );
    employeeFullName = `${employeeFullName[0].firstname} ${employeeFullName[0].lastname}`;

    let recordingTypeName = await sqlSelectsForLogs(
      "recordingType",
      timeRecording.recordingTypeId,
      "recordingTypeId",
      "recordingTypeName"
    );
    recordingTypeName = recordingTypeName[0].recordingTypeName;
    //console.log(recordingTypeName);

    console.log(timeRecording, timeRecording.duration, timeRecording.comment);

    let timeRecordingForLog = {
      employeeFullName: employeeFullName,
      projectName: projectName,
      recordingTypeName: recordingTypeName,
      duration: timeRecording.timeRecordingDuration,
      comment: timeRecording.timeRecordingComment,
    };

    //console.log("Löschen", timeRecordingForLog);
    await logEvent("Löschen", timeRecordingForLog);

    let projectId = await getProjectIdWithTimeRecordingId(timeRecordingId);

    //console.log(projectId);

    return new Promise((resolve, reject) => {
      connection.query(
        `DELETE FROM levi_timeRecordings WHERE timeRecordingId = ${timeRecordingId.id}`,
        (error, results, fields) => {
          if (error) {
            reject(error);
          } else {
            addRecordingIntoStatistic(projectId, 1);
            resolve(results);
          }
        }
      );
    });
  } catch (error) {
    console.error("Fehler:", error);
  }
}

async function addTimeRecordingModelDB(timeRecording) {
  let projectName = await sqlSelectsForLogs(
    "projects",
    timeRecording.projectId,
    "projectId",
    "projectName"
  );
  projectName = projectName[0].projectName;

  let employeeFullName = await sqlSelectsForLogs(
    "employees",
    timeRecording.employeeId,
    "employeeId",
    "firstname, lastname"
  );
  employeeFullName = `${employeeFullName[0].firstname} ${employeeFullName[0].lastname}`;

  let recordingTypeName = await sqlSelectsForLogs(
    "recordingType",
    timeRecording.recordingTypeId,
    "recordingTypeId",
    "recordingTypeName"
  );
  recordingTypeName = recordingTypeName[0].recordingTypeName;
  //console.log(recordingTypeName);

  console.log(timeRecording.duration, timeRecording.comment);

  let timeRecordingForLog = {
    employeeFullName: employeeFullName,
    projectName: projectName,
    recordingTypeName: recordingTypeName,
    duration: timeRecording.duration,
    comment: timeRecording.comment,
  };

  //console.log("Neuerfassung", timeRecordingForLog);
  await logEvent("Neuerfassung", timeRecordingForLog);
  return new Promise((resolve, reject) => {
    const sql = `INSERT INTO levi_timeRecordings (timeRecordingDuration, timeRecordingDate, timeRecordingComment, projectId, employeeId, recordingTypeId) 
      VALUES (
        ${timeRecording.duration}, 
        "${timeRecording.date}", 
        "${timeRecording.comment}", 
        ${timeRecording.projectId}, 
        ${timeRecording.employeeId}, 
        ${timeRecording.recordingTypeId}
        )`;
    connection.query(sql, (error, results, fields) => {
      if (error) {
        reject(error);
      } else {
        addRecordingIntoStatistic(timeRecording.projectId, 2);
        resolve(results.timeRecordingId);
      }
    });
  });
}
async function editTimeRecordingModelDB(timeRecording) {
  let projectName = await sqlSelectsForLogs(
    "projects",
    timeRecording.projectId,
    "projectId",
    "projectName"
  );
  projectName = projectName[0].projectName;

  let employeeFullName = await sqlSelectsForLogs(
    "employees",
    timeRecording.employeeId,
    "employeeId",
    "firstname, lastname"
  );
  employeeFullName = `${employeeFullName[0].firstname} ${employeeFullName[0].lastname}`;

  let recordingTypeName = await sqlSelectsForLogs(
    "recordingType",
    timeRecording.recordingTypeId,
    "recordingTypeId",
    "recordingTypeName"
  );
  recordingTypeName = recordingTypeName[0].recordingTypeName;
  //console.log(recordingTypeName);

  console.log(timeRecording.duration, timeRecording.comment);

  let timeRecordingForLog = {
    employeeFullName: employeeFullName,
    projectName: projectName,
    recordingTypeName: recordingTypeName,
    duration: timeRecording.duration,
    comment: timeRecording.comment,
  };
  //console.log("Editieren", timeRecordingForLog);
  await logEvent("Editieren", timeRecordingForLog);
  return new Promise((resolve, reject) => {
    const sql = `UPDATE levi_timeRecordings SET timeRecordingDuration = "${
      timeRecording.duration
    }", timeRecordingDate = "${formatDateDB(
      timeRecording.date
    )}", timeRecordingComment = "${timeRecording.comment}", projectId = ${
      timeRecording.projectId
    }, employeeId = ${timeRecording.employeeId}, recordingTypeId = ${
      timeRecording.recordingTypeId
    } WHERE timeRecordingId = ${timeRecording.timeRecordingId}`;
    connection.query(sql, (error, results, fields) => {
      if (error) {
        reject(error);
      } else {
        addRecordingIntoStatistic(timeRecording.projectId, 3);
        resolve(results.affectedRows > 0); // Gibt true zurück, wenn die Zeiterfassung bearbeitet wurde, sonst false
      }
    });
  });
}

function selectTimeRecordingsFromStat(
  projectId,
  startDateT,
  endDateT,
  statTypeId
) {
  const startDate = formatDateDB(startDateT);
  const endDate = formatDateDB(endDateT);
  //console.log(projectId, startDate, endDate);
  //console.log(statTypeId);
  return new Promise((resolve, reject) => {
    connection.query(
      `SELECT * FROM levi_statistics WHERE projectId = ${projectId} AND statTypeId = ${statTypeId} AND statDate >= '${startDate}' AND statDate <= '${endDate}';`,
      (error, results, fields) => {
        if (error) {
          reject(error);
        } else {
          resolve(results);
        }
      }
    );
  });
}

function addRecordingIntoStatistic(projectId, statTypeId) {
  let statDate = new Date();
  statDate = formatDateDB(statDate);
  console.log(statDate);
  return new Promise((resolve, reject) => {
    const sql = `INSERT INTO levi_statistics (statId, projectId, statTypeId, statDate) VALUES (null, ${projectId}, ${statTypeId}, '${statDate}');`;
    connection.query(sql, (error, results, fields) => {
      if (error) {
        reject(error);
      } else {
        resolve(results);
      }
    });
  });
}

function getRecordingsForCalcWEmployee(
  projectId,
  startDateT,
  endDateT,
  employeeId
) {
  const startDate = formatDateDB(startDateT);
  const endDate = formatDateDB(endDateT);
  return new Promise((resolve, reject) => {
    connection.query(
      `SELECT * FROM levi_timeRecordings WHERE projectId = ${projectId} AND employeeId = ${employeeId} AND timeRecordingDate >= '${startDate}' AND timeRecordingDate <= '${endDate}';`,
      (error, results, fields) => {
        if (error) {
          reject(error);
        } else {
          resolve(results);
        }
      }
    );
  });
}

function getRecordingsForCalcWRecordingType(
  projectId,
  startDateT,
  endDateT,
  recordingTypeId
) {
  const startDate = formatDateDB(startDateT);
  const endDate = formatDateDB(endDateT);
  return new Promise((resolve, reject) => {
    connection.query(
      `SELECT * FROM levi_timeRecordings WHERE projectId = ${projectId} AND recordingTypeId = ${recordingTypeId} AND timeRecordingDate >= '${startDate}' AND timeRecordingDate <= '${endDate}';`,
      (error, results, fields) => {
        if (error) {
          reject(error);
        } else {
          resolve(results);
        }
      }
    );
  });
}

function getAllRecordingsForCalcWithAll(projectId, startDateT, endDateT) {
  const startDate = formatDateDB(startDateT);
  const endDate = formatDateDB(endDateT);
  return new Promise((resolve, reject) => {
    connection.query(
      `SELECT * FROM levi_timeRecordings WHERE projectId = ${projectId} AND timeRecordingDate >= '${startDate}' AND timeRecordingDate <= '${endDate}';`,
      (error, results, fields) => {
        if (error) {
          reject(error);
        } else {
          resolve(results);
        }
      }
    );
  });
}

function sqlSelectsForLogs(table, id, whatId, searchedValue) {
  return new Promise((resolve, reject) => {
    connection.query(
      `SELECT ${searchedValue} FROM levi_${table} WHERE ${whatId} = ${id} OR ${whatId} = '${id}';`,
      (error, results, fields) => {
        if (error) {
          reject(error);
        } else {
          resolve(results);
        }
      }
    );
  });
}

function selectAllFromTimeRecWithId(timeRecordingId) {
  return new Promise((resolve, reject) => {
    connection.query(
      `SELECT * FROM levi_timeRecordings WHERE timeRecordingId = ${timeRecordingId} OR timeRecordingId = '${timeRecordingId}';`,
      (error, results, fields) => {
        if (error) {
          reject(error);
        } else {
          resolve(results);
        }
      }
    );
  });
}

function formatDateDB(dateString) {
  const date = new Date(dateString);
  const day = date.getDate();
  const month = date.getMonth() + 1;
  const year = date.getFullYear();

  // Führende Nullen für Tag und Monat hinzufügen, falls erforderlich
  const formattedDay = day < 10 ? "0" + day : day;
  const formattedMonth = month < 10 ? "0" + month : month;

  return year + "-" + formattedMonth + "-" + formattedDay;
}

export {
  editTimeRecordingModelDB,
  addTimeRecordingModelDB,
  deleteTimeRecordingModelDB,
  searchTimeRecordingsForProjectDB,
  readEmployeesDB,
  readProjectsDB,
  readRecordingTypesDB,
  selectTimeRecordingsFromStat,
  getRecordingsForCalcWEmployee,
  getRecordingsForCalcWRecordingType,
  getAllRecordingsForCalcWithAll,
};
