import express from "express";
import { router as dataRouter } from "./data/data.routes.js";
import { router as miscRouter } from "./misc/misc.routes.js";
import { router as calcRouter } from "./calculation/calculation.routes.js";
import { router as statRouter } from "./statistics/statistics.routes.js";
import cors from "cors";
import session from "express-session";
import Keycloak from "keycloak-connect";
import mysql from "mysql";
import jwt from "jsonwebtoken";

const checkRole = (requiredRoles) => {
  return (req, res, next) => {
    const token = req.headers.authorization.split(" ")[1];
    const decodedToken = jwt.decode(token);
    const roles = decodedToken.realm_access.roles;

    if (requiredRoles.some((role) => roles.includes(role))) {
      next();
    } else {
      res.status(403).json({ message: "Forbidden" });
    }
  };
};
const app = express();
const port = 3000;

app.use(cors());
app.use(express.json());

const memoryStore = new session.MemoryStore();
const keycloak = new Keycloak({ store: memoryStore });

app.use(keycloak.middleware());

app.use(
  session({
    secret: "vyyfgCpA2trBAPlqIapHjPgGKGi0id5w",
    resave: false,
    saveUninitialized: true,
    store: memoryStore,
  })
);

app.use("/api/data", keycloak.protect(), checkRole(["allRights"]), dataRouter);
app.use(
  "/api/misc",
  keycloak.protect(),
  checkRole(["allRights", "onlyStatistics"]),
  miscRouter
);
app.use("/api/calc", keycloak.protect(), checkRole(["allRights"]), calcRouter);
app.use(
  "/api/stat",
  keycloak.protect(),
  checkRole(["allRights", "onlyStatistics"]),
  statRouter
);

app.get("/", (req, res) => {
  res.send("Welcome to my server!");
});

app.listen(port, () => {
  console.log(`Server is running on port ${port}`);
});
