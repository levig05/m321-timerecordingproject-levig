import {
  readEmployeesDB,
  readProjectsDB,
  readRecordingTypesDB,
} from "../services/database.js";

const employees = [
  { firstname: "John", lastname: "Doe", id: 1 },
  { firstname: "Simeon", lastname: "Stix", id: 2 },
  { firstname: "Martin", lastname: "Hager", id: 3 },
  { firstname: "Max", lastname: "Foll", id: 4 },
];

const projects = [
  { name: "Project 1", id: "1" },
  { name: "Project 2", id: "2" },
  { name: "Project 3", id: "3" },
  { name: "Project 4", id: "4" },
];

const recordingTypes = [
  { name: "Entwicklung", id: 1 },
  { name: "Debugging", id: 2 },
  { name: "Testing", id: 3 },
  { name: "Fertigstellung", id: 4 },
];

async function readEmployees() {
  return await readEmployeesDB();
}

async function readProjects() {
  return await readProjectsDB();
}

async function readRecordingTypes() {
  return await readRecordingTypesDB();
}

/*
function readEmployees() {
  return employees;
}

function readProjects() {
  return projects;
}

function readRecordingTypes() {
  return recordingTypes;
}
*/

export { readEmployees, readProjects, readRecordingTypes };
