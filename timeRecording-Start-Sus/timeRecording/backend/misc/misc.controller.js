import {
  readEmployees,
  readProjects,
  readRecordingTypes,
} from "./misc.model.js";

async function readAllEmployees(request, response) {
  console.log("readAllEmployees");
  const employees = await readEmployees();
  console.log("employees read: ", employees);
  response.json(employees);
}

async function readAllProjects(request, response) {
  console.log("readAllProjects");
  const projects = await readProjects();
  console.log("projects read: ", projects);
  response.json(projects);
}

async function readAllRecordingTypes(request, response) {
  console.log("readAllRecordingTypes");
  const types = await readRecordingTypes();
  console.log("projects read: ", types);
  response.json(types);
}

export { readAllEmployees, readAllProjects, readAllRecordingTypes };
