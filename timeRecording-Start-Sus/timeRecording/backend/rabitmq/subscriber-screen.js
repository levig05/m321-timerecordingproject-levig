import amqp from "amqplib/callback_api.js";
import fs from "fs";
import path from "path";

const EXCHANGE_NAME = "log_levi_grossenbacher";
const EXCHANGE_TYPE = "fanout";
const LOG_DIR = "./logs";
const LOG_FILE = path.join(
  LOG_DIR,
  `log_${new Date().toISOString().split("T")[0]}_levi_grossenbacher.txt`
);

let eventLog = [];

// Funktion, um das Logfile zu lesen und die letzten 20 Nachrichten anzuzeigen
const loadLogFile = () => {
  if (fs.existsSync(LOG_FILE)) {
    const data = fs.readFileSync(LOG_FILE, "utf8");
    eventLog = data.trim().split("\n");
    if (eventLog.length > 20) {
      eventLog = eventLog.slice(-20); // nur die letzten 20 Nachrichten behalten
    }
  } else {
    console.log("Logfile nicht gefunden, wird erstellt.");
  }
  logToScreen();
};

// Funktion, um Nachrichten ins Logfile zu schreiben
const appendToLogFile = (msg) => {
  if (!fs.existsSync(LOG_DIR)) {
    fs.mkdirSync(LOG_DIR);
  }
  fs.appendFileSync(LOG_FILE, msg + "\n");
};

const logToScreen = () => {
  console.clear();
  console.log("Latest 20 events:");
  eventLog.forEach((event, index) => {
    console.log(`${index + 1}. ${event}`);
  });
};

amqp.connect("amqp://localhost", (err, connection) => {
  if (err) throw err;
  connection.createChannel((err, channel) => {
    if (err) throw err;

    channel.assertExchange(EXCHANGE_NAME, EXCHANGE_TYPE, { durable: false });

    channel.assertQueue("", { exclusive: true }, (err, q) => {
      if (err) throw err;
      console.log("Waiting for messages in queue");

      channel.bindQueue(q.queue, EXCHANGE_NAME, "");

      // Lade die letzten 20 Nachrichten aus dem Logfile beim Start
      loadLogFile();

      channel.consume(
        q.queue,
        (msg) => {
          if (msg.content) {
            const message = msg.content.toString();
            eventLog.push(message);
            if (eventLog.length > 20) {
              eventLog.shift(); // Entferne die älteste Nachricht
            }
            appendToLogFile(message);
            logToScreen();
          }
        },
        { noAck: true }
      );
    });
  });
});
