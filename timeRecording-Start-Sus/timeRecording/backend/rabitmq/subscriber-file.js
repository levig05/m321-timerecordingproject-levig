import amqp from "amqplib/callback_api.js";
import fs from "fs";
import path from "path";
import moment from "moment";

const EXCHANGE_NAME = "log_levi_grossenbacher";
const EXCHANGE_TYPE = "fanout";

const logToFile = (msg) => {
  const logDir = "./logs";
  if (!fs.existsSync(logDir)) {
    fs.mkdirSync(logDir);
  }

  const fileName = `log_${moment().format(
    "YYYY-MM-DD"
  )}_levi_grossenbacher.txt`;
  const filePath = path.join(logDir, fileName);

  fs.appendFile(filePath, `${msg}\n`, (err) => {
    if (err) throw err;
    console.log(`Message logged to ${filePath}`);
  });
};

amqp.connect("amqp://localhost", (err, connection) => {
  if (err) throw err;
  connection.createChannel((err, channel) => {
    if (err) throw err;

    channel.assertExchange(EXCHANGE_NAME, EXCHANGE_TYPE, { durable: false });

    channel.assertQueue("", { exclusive: true }, (err, q) => {
      if (err) throw err;
      console.log("Waiting for messages in queue");

      channel.bindQueue(q.queue, EXCHANGE_NAME, "");

      channel.consume(
        q.queue,
        (msg) => {
          if (msg.content) {
            logToFile(msg.content.toString());
          }
        },
        { noAck: true }
      );
    });
  });
});
