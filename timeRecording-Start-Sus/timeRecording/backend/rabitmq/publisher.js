import amqp from "amqplib/callback_api.js";

const EXCHANGE_NAME = "log_levi_grossenbacher";
const EXCHANGE_TYPE = "fanout";

const publishEvent = (event) => {
  amqp.connect("amqp://localhost", (err, connection) => {
    if (err) throw err;
    connection.createChannel((err, channel) => {
      if (err) throw err;

      channel.assertExchange(EXCHANGE_NAME, EXCHANGE_TYPE, {
        durable: false,
      });

      const msg = JSON.stringify(event);
      channel.publish(EXCHANGE_NAME, "", Buffer.from(msg));
      //console.log(`Event published: ${msg}`);

      setTimeout(() => {
        connection.close();
      }, 500);
    });
  });
};

export { publishEvent };
