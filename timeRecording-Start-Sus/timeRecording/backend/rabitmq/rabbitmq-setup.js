import amqp from "amqplib/callback_api";

const EXCHANGE_NAME = "log_levi_grossenbacher";
const EXCHANGE_TYPE = "fanout";

amqp.connect("amqp://localhost", (err, connection) => {
  if (err) throw err;
  connection.createChannel((err, channel) => {
    if (err) throw err;

    channel.assertExchange(EXCHANGE_NAME, EXCHANGE_TYPE, {
      durable: false,
    });

    console.log(`Exchange ${EXCHANGE_NAME} created`);
    setTimeout(() => {
      connection.close();
    }, 500);
  });
});
